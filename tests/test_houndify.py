import unittest
import houndify
import csv

class TestClient(unittest.TestCase):
    def setUp(self):
        self.CLIENT_ID = "IEy9bFbJTipCCG9SkuoMXg=="
        self.CLIENT_KEY = "KIzMNJI4zFrtj6XnOd3vKByzIr35rGj1j1gbVBKQbpUlFh9QM1QxHCpLIa9VkbXr9-JTD5EjT0jynw1amYTkaA=="
        self.requestInfo = {
                         ## Pretend we're at SoundHound HQ.  Set other fields as appropriate
                         'Latitude': 37.388309, 
                         'Longitude': -121.973968
                           }
        self.client = houndify.TextHoundClient(self.CLIENT_ID, self.CLIENT_KEY, "test_user", self.requestInfo)
        self.verificationErrors = []
    
    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

    def test_output(self):
        with open('TestInput.csv', 'r') as f:
            reader = csv.reader(f)
            input_list = list(reader)
        for item in range(2, len(input_list)-1):
            self.response = self.client.query(input_list[item][0])
            # self.assertEqual(self.response['AllResults'][0]['CommandKind'], input_list[item][1])
            try: 
                self.assertEqual(self.response['AllResults'][0]['CommandKind'], input_list[item][1])
            except AssertionError as e:
                self.verificationErrors.append(str(e))
            except KeyError as e:
                self.verificationErrors.append(str(e))
            else:
                print("Test_{} - Expected Result- {} -- Actual Result- {} - PASSED".format(input_list[item][4], input_list[item][1], self.response['AllResults'][0]['CommandKind']))

            if self.response['AllResults'][0]['CommandKind'] == 'InformationCommand':
                try:
                    self.assertEqual(self.response['AllResults'][0]['InformationNuggets'][0]['NuggetKind'], input_list[item][2])
                except AssertionError as e:
                    self.verificationErrors.append(str(e))
                except KeyError as e:
                    self.verificationErrors.append(str(e))
                else:
                    print("Test_{} - Expected Result- {} -- Actual Result- {} - PASSED".format(input_list[item][4], input_list[item][2], self.response['AllResults'][0]['InformationNuggets'][0]['NuggetKind']))

                try:
                    self.assertEqual(self.response['AllResults'][0]['InformationNuggets'][0]['WeatherNuggetKind'], input_list[item][3])
                except AssertionError as e:
                    self.verificationErrors.append(str(e))
                except KeyError as e:
                    self.verificationErrors.append(str(e))
                else:
                    print("Test_{} - Expected Result- {} -- Actual Result- {} - PASSED".format(input_list[item][4], input_list[item][3], self.response['AllResults'][0]['InformationNuggets'][0]['WeatherNuggetKind']))
        


